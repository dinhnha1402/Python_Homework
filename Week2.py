import math


'''
HOMEWORK - WEEK 2
Ex1: Given the following statement: u, v, x, y, z = 29, 12, 10, 4, 3. write result of the following expression:
'''

print("")
print("Exercise 1:")
u, v, x, y, z = 29, 12, 10, 4, 3

a = u/v
b = (u==v)
c = u % x
d = t = (x >= y)
e = u = u + 5
f = u = u % z
g = t = (v > x and y < z)
h = x**z
i = x // z

print("a. u/v =",a)
print("b. t = (u == v)  ⇒ t = ",b)
print("c. u % x = ",c)
print("d. t = (x >= y)  ⇒ t = ",d)
print("e. u += 5  ⇒ u = ",e)
print("f. u %=z ⇒ u = ",f)
print("g. t = (v > x and y < z)  ⇒ t = ",g)
print("h. x**z = ",h)
print("i. x // z = ",i)

'''
Ex2: Given a string s = “Hi John, welcome to python programming for beginner!”. Your mission is write a small python script to:
Check a string “python” that either exists in s or not.
Extract the word “John” from s and save it into a variable named s1.
Count how many word in s. 
Guide:  use split() function of string to split s to a list of strings and then use len() function to count the size of list.
'''

print("")
print("Exercise 2:")
s = 'Hi John, welcome to python programming for beginner!'
check = 'python'
s1 = 0

if check in s:
    print('Python is exists in s')
else:
    print('Python is not exists in s')

s2,s3 = s.split(", welcome to python programming for beginner!")
s4,s1 = s2.split("Hi ")
print(s1)

countword = s.count(' ')

print("Words in s = ",countword+1)

'''
Ex3: Write a python script to compute the perimeter and the area of a circle with radius r = 5. 
'''

print("")
print("Exercise 3:")
r = 5

S = r*r * math.pi
print("S = ",round(S, 2))

C = r * 2 * math.pi
print("C = ",round(C, 2))

'''
Ex4:
Write a python script to print the following string in a specific format. s = “Twinkle, twinkle, little star, How I wonder what you are! Up above the world so high, Like a diamond in the sky. Twinkle, twinkle, little star, How I wonder what you are”.  The output is: 
Twinkle, twinkle, little star,
How I wonder what you are!
Up above the world so high,
Like a diamond in the sky.
Twinkle, twinkle, little star,
How I wonder what you are.
'''

print("")
print("Exercise 4:")

song = 'Twinkle, twinkle, little star, How I wonder what you are! Up above the world so high, Like a diamond in the sky. Twinkle, twinkle, little star, How I wonder what you are.'


print(song[0:30])
print("        " + song[31:57])
print(song[58:85])
print("        " + song[86:112])
print(song[113:144])
print("        " + song[144:170])

'''
Ex5:
Given a list as follows: l = [23, 4.3, 4.2, 31, ‘python’, 1, 5.3, 9, 1.7]
Remove the item “python” in the list.
Sort this list by ascending and descending.
Check either number 4.2 to be in l or not? 
'''

print("")
print("Exercise 5:")

l = [23, 4.3, 4.2, 31, 'python', 1, 5.3, 9, 1.7]

l.remove('python')
print('a. l = ' ,l)

l.sort()
print('b. Ascending = ',l)

l.reverse()
print('   Descending = ',l)


if 4.2 in l:
    print("c. Yes number 4.2 in l")
else:
    print("c. No number 4.2 not in l")