import requests
import os
import sys
from urllib.parse import urlparse


def download_web_page():
    if len(sys.argv) != 2:
        print("Not enough or invalid arguments")
        sys.exit(1)

    url = sys.argv[1]
    base_name = os.path.basename(urlparse(url).path)

    if not base_name:
        file_name = 'index.html'
        index = 2
        # check file is existed or not
        while os.path.isfile(file_name):
            file_name = 'index_' + str(index) + '.html'
            index += 1
        # write file
        with open(file_name, 'wb') as f:
            resp = requests.get(url)
            if resp.status_code == 200:
                print('saving ' + url + ' as ' + file_name)
                f.write(resp.content)
    else:
        with open(base_name, 'wb') as f:
            resp = requests.get(url)
            if resp.status_code == 200:
                print('saving ' + url + ' as ' + base_name)
                f.write(resp.content)


if __name__ == '__main__':
    download_web_page()
