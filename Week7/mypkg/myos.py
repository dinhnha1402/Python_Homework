import os


def show_content(path, level):
    os.chdir(path)
    indent = ' ' * 4 * level + '|-- '
    sub_indent = ' ' * 4 * (level + 1) + '|-- '
    print('{}{}'.format(indent, os.path.basename(path)))
    l = os.listdir(path)

    for f in l:
        if os.path.isfile(f):
            print('{}{}'.format(sub_indent, f))

    directories = [d for d in l if os.path.isdir(d)]
    if not directories:
        return
    else:
        for d in directories:
            show_content(os.path.join(path, d), level + 1)


def show_content_and_file_size(path, level):
    os.chdir(path)
    indent = ' ' * 4 * level + '|-- '
    sub_indent = ' ' * 4 * (level + 1) + '|-- '

    print('{}{}'.format(indent, os.path.basename(path)))
    l = os.listdir(path)

    for f in l:
        if os.path.isfile(f):
            size = os.path.getsize(f)
            if size >= 1024:
                size = str(round(size/1024, 2)) + ' Kb'
            else:
                size = str(size) + ' b'
            print(str(sub_indent + f).ljust(100) + size)

    directories = [d for d in l if os.path.isdir(d)]
    if not directories:
        return
    else:
        for d in directories:
            show_content_and_file_size(os.path.join(path, d), level + 1)


def use_show_content():
    show_content(input('Enter a path: '), 0)


def use_show_content_and_file_size():
    show_content_and_file_size(input("Enter a path: "), 0)


# Using os.walk
# def list_files(startpath):
#     for root, dirs, files in os.walk(startpath):
#         level = root.replace(startpath, '').count(os.sep)
#         indent = ' ' * 4 * level
#         print('{}{}'.format(indent, os.path.basename(root)))
#         subindent = ' ' * 4 * (level + 1)
#         for f in files:
#             print('{}{}'.format(subindent, f))
#
# list_files(input("Enter a path: "));
